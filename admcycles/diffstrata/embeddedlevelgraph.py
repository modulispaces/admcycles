from collections import defaultdict
from copy import deepcopy
import itertools

# pylint does not know sage
from sage.structure.sage_object import SageObject  # pylint: disable=import-error
from sage.matrix.constructor import matrix  # pylint: disable=import-error
from sage.misc.flatten import flatten  # pylint: disable=import-error
from sage.rings.rational_field import QQ  # pylint: disable=import-error
from sage.arith.functions import lcm  # pylint: disable=import-error
from sage.misc.cachefunc import cached_method  # pylint: disable=import-error

import admcycles.diffstrata
from admcycles.diffstrata.sig import Signature


class EmbeddedLevelGraph(SageObject):
    r"""
    LevelGraph inside a generalised stratum.

    Note that the points of the enveloping GeneralisedStratum are of the form
    (i,j) where i is the component and j the index of sig of that component,
    while the points of the level graph are numbers 1,...,n.

    Thus, dmp is a dictionary mapping integers to tuples of integers.

    Attributes:

    * LG (LevelGraph): underlying LevelGraph
    * X (GeneralisedStratum): enveloping stratum
    * dmp (dict): (bijective!) dictionary marked points of LG -> points of stratum
    * dmp_inv (dict): inverse of dmp
    * dlevels (dict): (bijective!) dictionary levels of LG -> new level numbering
    * dlevels_inv (dict): inverse of dlevels
    * top (GeneralisedStratum): (if self is a BIC) top component
    * bot (GeneralisedStratum): (if self is a BIC) bottom component
    * clutch_dict (dict): (if self is a BIC) dictionary mapping points of top
      stratum to points of bottom stratum where there is an edge in self.
    * emb_top (dict): (if self is a BIC) dictionary mapping points of stratum top
      to the corresponding points of the enveloping stratum.
    * emb_bot (dict): (if self is a BIC) dictionary mapping points of stratum bot
      to the corresponding points of the enveloping stratum.
    * automorphisms (list of list of dicts): automorphisms
    * codim (int): codimension of LevelGraph in stratum
      number_of_levels (int): Number of levels of self.

    Note that attempting to access any of the attributes top, bot, clutch_dict,
    emb_top or emb_bot will raise a ValueError if self is not a BIC.
    """

    def __init__(self, X, LG, dmp, dlevels):
        r"""
        Initialises EmbeddedLevelGraph.

        Args:
            LG (LevelGraph): underlying LevelGraph
            X (GeneralisedStratum): enveloping stratum
            dmp (dictionary): (bijective!) dictionary marked points of LG -> points of stratum
            dlevels (dictionary): (bijective!) dictionary levels of LG -> new level numbering
        """
        self.LG = LG
        self.X = X
        self.dmp = dmp
        self.dmp_inv = {value: key for key, value in dmp.items()}
        self.dlevels = dlevels
        self.dlevels_inv = {value: key for key, value in dlevels.items()}
        self._top = None
        self._bot = None
        self._clutch_dict = None
        self._emb_top = None
        self._emb_bot = None
        self.codim = self.LG.codim()
        self.number_of_levels = len(set(self.dlevels.keys()))

        if LG.deck is not None:
            for l, p in self.dmp.items():
                assert dmp[self.LG.deck[l]] == self.X.deck[p]

    def __repr__(self):
        return "EmbeddedLevelGraph(LG=%r,dmp=%r,dlevels=%r)" % (
            self.LG, self.dmp, self.dlevels)

    def __str__(self):
        return (
            "Embedded Level Graph consisting of %s with point dictionary %s and level dictionary %s" %
            (self.LG, self.dmp, self.dlevels))

    def explain(self):
        r"""
        A more user-friendly display of __str__ :-)
        """
        def _list_print(L):
            if len(L) > 1:
                s = ['s ']
                s.extend('%r, ' % x for x in L[:-2])
                s.append('%r ' % L[-2])
                s.append('and %r.' % L[-1])
                return ''.join(s)
            else:
                return ' %r.' % L[0]

        def _num(i):
            if i == 1:
                return 'one edge'
            else:
                return '%r edges' % i
        print("LevelGraph embedded into stratum %s with:" % self.X)
        LG = self.LG
        for l in range(LG.numberoflevels()):
            internal_l = LG.internal_level_number(l)
            print("On level %r:" % l)
            for v in LG.verticesonlevel(internal_l):
                print("* A vertex (number %r) of genus %r" % (v, LG.genus(v)))
        levels_of_mps = list(
            {LG.level_number(LG.levelofleg(leg)) for leg in self.dmp})
        print("The marked points are on level%s" %
              _list_print(sorted(levels_of_mps)))
        print("More precisely, we have:")
        for leg in self.dmp:
            print(
                "* Marked point %r of order %r on vertex %r on level %r" %
                (self.dmp[leg],
                 LG.orderatleg(leg),
                 LG.vertex(leg),
                    LG.level_number(
                    LG.levelofleg(leg))))
        print("Finally, we have %s. More precisely:" % _num(len(LG.edges)))
        edge_dict = {e: (LG.vertex(e[0]), LG.vertex(e[1])) for e in LG.edges}
        edge_dict_inv = {}
        for k, v in edge_dict.items():
            if v in edge_dict_inv:
                edge_dict_inv[v].append(k)
            else:
                edge_dict_inv[v] = [k]
        for e in edge_dict_inv:
            print("* %s between vertex %r (on level %r) and vertex %r (on level %r) with prong%s" %
                  (_num(len(edge_dict_inv[e])),
                   e[0], LG.level_number(LG.levelofvertex(e[0])),
                   e[1], LG.level_number(LG.levelofvertex(e[1])),
                   # _write_prongs()
                   _list_print([LG.prong(ee) for ee in edge_dict_inv[e]])))

    def __eq__(self, other):
        if not isinstance(other, EmbeddedLevelGraph):
            return False
        return self.LG == other.LG and self.dmp == other.dmp and self.dlevels == other.dlevels

    def is_bic(self):
        return self.LG.is_bic()

    @property
    @cached_method
    def ell(self):
        r"""
        If self is a BIC: the lcm of the prongs.

        Raises:
            RuntimeError: raised if self is not a BIC.

        Returns:
            int: lcm of the prongs.
        """
        if not self.is_bic():
            raise RuntimeError("ell only defined for BICs!")
        return lcm(kappa for kappa in self.LG.prongs.values())

    # Dawei's positivity coefficients
    @property
    def b(self):
        if self.X.k != 1:
            raise NotImplementedError("Not implemented for k != 1")
        if self.X._h0 > 1:
            raise ValueError('Cannot compute b on disconnected stratum.')
        g = self.X._sig_list[0].g
        val = 0
        # super inefficient, but probably good enough for now:
        # take the underlying StableGraph and, for each edge,
        # contract all other edges and check which graph we end up with:
        stgraph = self.LG.stgraph
        for e in self.LG.edges:
            ee = self.LG.edges[:]
            ee.remove(e)
            curr_graph = stgraph.copy()
            for contr in ee:
                curr_graph.contract_edge(contr)
            assert curr_graph.edges() == [e]
            if len(curr_graph.genera()) == 2:
                # compact type:
                i = min(curr_graph.genera())
                val += QQ(6 * i * (g - i)) / QQ((g + 3) * self.LG.prong(e))
            else:
                # irreducible
                assert len(curr_graph.genera()) == 1
                val += QQ(g + 1) / QQ((g + 3) * self.LG.prong(e))
        return self.ell * val

    @property
    def c(self):
        if self.X.k != 1:
            raise NotImplementedError("Not implemented for k != 1")
        return self.ell * (self.bot.kappa_EKZ -
                           self.X.kappa_EKZ * QQ(self.bot.N) / QQ(self.X.N))
    ######

    @property
    def top(self):
        if self._top is None:
            self.split()
        return self._top

    @property
    def bot(self):
        if self._bot is None:
            self.split()
        return self._bot

    @property
    def clutch_dict(self):
        if self._clutch_dict is None:
            self.split()
        return self._clutch_dict

    @property
    def emb_bot(self):
        if self._emb_bot is None:
            self.split()
        return self._emb_bot

    @property
    def emb_top(self):
        if self._emb_top is None:
            self.split()
        return self._emb_top

    def free_poles(self):
        r"""
        Return a list of all free poles (i.e. poles that are not part
        of a residue condition) of self.
        """
        legs_in_res = [self.dmp_inv[p] for rc in self.X.res_cond for p in rc]
        return [l for l in self.LG.list_markings()
                if self.LG.poleorders[l] < 0 and
                l not in legs_in_res]

    def stratum_from_level(self, l, canonicalize=True):
        r"""
        Return the LevelStratum at (relative) level l.

        INPUT:

        l (int): relative level number (0,...,codim)

        OUTPUT:

        LevelStratum: the LevelStratum, i.e.

        * a list of Signatures (one for each vertex on the level)
        * a list of residue conditions, i.e. a list [res_1,...,res_r]
          where each res_k is a list of tuples [(i_1,j_1),...,(i_n,j_n)]
          where each tuple (i,j) refers to the point j (i.e. index) on the
          component i and such that the residues at these points add up
          to 0.
        * a dictionary of legs, i.e. n -> (i,j) where n is the original
          number of the point (on the LevelGraph self) and i is the
          number of the component, j the index of the point in the signature tuple.

        Note that LevelStratum is a GeneralisedStratum together with
        a leg dictionary.
        """
        if self.LG.is_horizontal():
            raise NotImplementedError("Cannot extract levels of a graph with horizontal edges!")
        internal_l = self.LG.internal_level_number(l)
        # first, we extract the obvious information from self at this level:
        vv = self.LG.verticesonlevel(internal_l)
        legs_on_level = []
        sigs = []
        res_cond = []
        leg_dict = {}
        for i, v in enumerate(vv):
            legs_on_level += [deepcopy(self.LG.legsatvertex(v))]
            leg_dict.update([(n, (i, j))
                             for j, n in enumerate(legs_on_level[-1])])
            sig_v = Signature(tuple(self.LG.orderatleg(leg)
                                    for leg in legs_on_level[-1]))
            sigs += [sig_v]
        # Now we construct the deck transformation for the new signature
        deck = {}
        for n, p in leg_dict.items():
            deck[p] = leg_dict[self.LG.deck[n]]
        # Now we hunt for residue conditions.
        # We first build an adjacency list for the vertices above level l,
        # including the vertices at infinity.
        # We represent each vertex by its adjacent half-edges.
        verts_above_l = [ls for i, ls in enumerate(self.LG.legs) if self.LG.levels[i] > internal_l]
        # For each residue condition, we add a vertex and pseude-edges
        next_l = max(max(ls) for ls in self.LG.legs) + 1
        inf_edges = []
        for rc in self.X.res_cond:
            rc_legs = [self.dmp_inv[p] for p in rc]
            v = []
            for l in rc_legs:
                v.append(next_l)
                inf_edges.append((next_l, l))
                next_l += 1
            verts_above_l.append(v)
        legs_to_verts = {l: i for i, ls in enumerate(verts_above_l) for l in ls}
        adjacency = defaultdict(list)
        for (l1, l2) in self.LG.edges + inf_edges:
            if l1 in legs_to_verts and l2 in legs_to_verts:
                adjacency[legs_to_verts[l1]].append(legs_to_verts[l2])
                adjacency[legs_to_verts[l2]].append(legs_to_verts[l1])

        conn_comps = []
        to_handle = set(range(len(verts_above_l)))
        while to_handle:
            v = to_handle.pop()
            current = [v]
            current_new = [v]
            while current_new:
                v = current_new.pop()
                for vv in adjacency[v]:
                    if vv in current:
                        continue
                    else:
                        current.append(vv)
                        current_new.append(vv)
                        to_handle.remove(vv)
            conn_comps.append(current)

        free_poles = self.free_poles()
        has_free_pole = [any(l in free_poles for v in cc for l in verts_above_l[v]) for cc in conn_comps]

        legs_to_cc = {l: i for i, cc in enumerate(conn_comps) for v in cc for l in verts_above_l[v]}
        all_legs_on_level = flatten(legs_on_level)
        # Now assemble the residue conditions
        res_cond = [[] for _ in conn_comps]
        for (l1, l2) in self.LG.edges + inf_edges:
            if l2 in all_legs_on_level:
                assert l1 in legs_to_verts
                if has_free_pole[legs_to_cc[l1]]:
                    continue
                res_cond[legs_to_cc[l1]].append(leg_dict[l2])
        res_cond = [rc for rc in res_cond if rc]
        return admcycles.diffstrata.generalisedstratum.LevelStratum(
            sigs, self.LG.cover_of_k, deck, res_cond, leg_dict, canonicalize)

    @property
    def residue_matrix_from_RT(self):
        r"""
        The matrix associated to the residue conditions imposed by the residue theorem
        on each vertex of self.

        Returns:
            SAGE Matrix: matrix of residue conditions given by RT
        """
        poles_by_vertex = {}
        for p in self.X._polelist:
            vertex = self.LG.vertex(self.dmp_inv[p])
            try:
                poles_by_vertex[vertex].append(p)
            except KeyError:
                poles_by_vertex[vertex] = [p]
        rows = [[int(p in poles_by_vertex[v]) for p in self.X._polelist]
                for v in poles_by_vertex]
        return matrix(QQ, rows)

    @property
    def full_residue_matrix(self):
        r"""
        Residue matrix with GRC conditions and RT conditions (for each vertex).

        OUTPUT:

        A matrix with number of poles columns and a row for each condition.
        """
        M = self.X.residue_matrix()
        if M:
            M = M.stack(self.residue_matrix_from_RT)
        else:
            M = self.residue_matrix_from_RT
        return M

    def residue_zero(self, pole):
        r"""
        Check if the residue at pole is forced zero by residue conditions.

        NOTE: We DO include the RT on the vertices in this check!

        Args:
            pole (tuple): pole (as a point (i,j) of self.X)

        Returns:
            bool: True if forced zero, False otherwise.
        """
        # add the equation corresponding to the residue at pole to the residue matrix
        # and see if the rank changes:
        i = self.X._polelist.index(pole)
        res_vec = [[int(i == j) for j in range(len(self.X._polelist))]]
        RM = self.full_residue_matrix
        # RM = self.X.residue_matrix()
        if RM:
            stacked = RM.stack(matrix(res_vec))
            return stacked.rank() == self.full_residue_matrix.rank()
            # return stacked.rank() == self.X.residue_matrix().rank()
        else:
            return False

    @cached_method
    def level(self, l):
        r"""
        The generalised stratum on level l.

        Note that this is cached, i.e. on first call, it is stored in the dictionary
        _level.

        INPUT:

        l: integer
        The relative level number (0,...,codim)

        OUTPUT:

        The LevelStratum that is

        * a list of Signatures (one for each vertex on the level)
        * a list of residue conditions, i.e. a list [res_1,...,res_r]
          where each res_k is a list of tuples [(i_1,j_1),...,(i_n,j_n)]
          where each tuple (i,j) refers to the point j (i.e. index) on the
          component i and such that the residues at these points add up
          to 0.
        * a dictionary of legs, i.e. n -> (i,j) where n is the original
          number of the point (on the LevelGraph self) and i is the
          number of the component, j the index of the point in the signature tuple.

        Note that LevelStratum is a GeneralisedStratum together with
        a leg dictionary. Here, we provide an additional attribute:

        * leg_orbits, a nested list giving the orbits of the points on the level
          under the automorphism group of self.
        """
        return self.stratum_from_level(l)

    def noncanonical_level(self, l):
        r"""
        The generalised stratum on level l, but without canonicalization.
        DO NOT USE THIS. Use `level()` instead.
        This is only here because I'm to stupid to fix to_prodtautclass for the
        canonical levels.

        INPUT:

        l: integer
        The relative level number (0,...,codim)

        OUTPUT:

        The LevelStratum.
        """
        return self.stratum_from_level(l, False)

    def edge_orbit(self, edge):
        r"""
        The edge orbit of edge in self.

        raises a ValueError if edge is not an edge of self.LG

        INPUT:

        edge: tuple
        An edge of ``self.LG``, i.e. tuple (start leg, end leg), where start
        leg should not be on a lower level than end leg.

        OUTPUT:

        The set of edges in automorphism orbit of ``edge``.
        """
        if edge not in self.LG.edges:
            raise ValueError("%r is not an edge of %r!" % (edge, self))
        s = {edge}
        for v_map, l_map in self.automorphisms:
            new_edge = (l_map[edge[0]], l_map[edge[1]])
            s.add(new_edge)
        return s

    def len_edge_orbit(self, edge):
        r"""
        Length of the edge orbit of edge in self.

        Args:
            edge (tuple): edge of self.LG, i.e. tuple (start leg, end leg), where
                start leg should not be on a lower level than end leg.

        Raises:
            ValueError: if edge is not an edge of self.LG

        Returns:
            int: length of the aut-orbit of edge.

        EXAMPLES::


            Prongs influence the orbit length.

        """
        return len(self.edge_orbit(edge))

    def automorphisms_stabilising_legs(self, leg_tuple):
        stabs = []
        for v_map, l_map in self.automorphisms:
            for l in leg_tuple:
                if l_map[l] != l:
                    break
            else:  # no break
                stabs.append(l_map)
        return stabs

    def delta(self, i):
        r"""
        Squish all levels except for i.

        Note that delta(1) contracts everything except top-level and that the
        argument is interpreted via internal_level_number (i.e. a relative level number).

        Moreover, dlevels is set to map to 0 and -1(!).

        Args:
            i (int): Level not to be squished.

        Returns:
            EmbeddedLevelGraph: Embedded BIC (result of applying delta to the
                underlying LevelGraph)
        """
        newLG = self.LG.delta(i, quiet=True)
        newdmp = self.dmp.copy()
        # level_number is (positive!) relative level number.
        newdlevels = {l: -newLG.level_number(l) for l in newLG.levels}
        return EmbeddedLevelGraph(self.X, newLG, newdmp, newdlevels)

    def squish_vertical(self, level):
        r"""
        Squish level crossing below level 'level'.

        Note that in contrast to the levelgraph method, we work with relative
        level numbers here!

        Args:
            level (int): relative (!) level number.

        Returns:
            EmbeddedLevelGraph: Result of squishing.

        EXAMPLES::

            sage: from admcycles.diffstrata import *
            sage: X=GeneralisedStratum([Signature((4,))])
            sage: p = X.enhanced_profiles_of_length(4)[0][0]
            sage: g = X.lookup_graph(p)

            lookup_graph uses the sorted profile (note that these do not have to be reduced!):

            sage: assert any(g.squish_vertical(0).is_isomorphic(G) for G in X.lookup(p[1:]))
            sage: assert any(g.squish_vertical(1).is_isomorphic(G) for G in X.lookup(p[:1]+p[2:]))
            sage: assert any(g.squish_vertical(2).is_isomorphic(G) for G in X.lookup(p[:2]+p[3:]))
            sage: assert any(g.squish_vertical(3).is_isomorphic(G) for G in X.lookup(p[:3]))

            Squishing outside the range of levels does nothing:

            sage: assert g.squish_vertical(4) == g

            Recursive squishing removes larger parts of the profile:

            sage: assert any(g.squish_vertical(3).squish_vertical(2).is_isomorphic(G) for G in X.lookup(p[:2]))
        """
        newLG = self.LG.squish_vertical(
            self.LG.internal_level_number(level), quiet=True)
        newdmp = self.dmp.copy()
        # level_number is (positive!) relative level number.
        newdlevels = {l: -newLG.level_number(l) for l in newLG.levels}
        return EmbeddedLevelGraph(self.X, newLG, newdmp, newdlevels)

    def split(self):
        r"""
        Splits embedded BIC self into top and bottom component.

        Raises a ValueError if self is not a BIC.

        OUTPUT:

        A dictionary consising of

        * the GeneralisedStratum self.X
        * the top component LevelStratum
        * the bottom component LevelStratum
        * the clutching dictionary mapping ex-half-edges on
          top to their partners on bottom (both as points in the
          respective strata!)
        * a dictionary embedding top into the stratum of self
        * a dictionary embedding bot into the stratum of self

        Note that clutch_dict, emb_top and emb_bot are dictionaries between
        points of strata, i.e. after applying dmp to the points!
        """
        if not self.is_bic():
            raise ValueError(
                "Error: %s is not a BIC! Cannot be split into Top and Bottom component!" %
                self)
        self._top = self.level(0)
        self._bot = self.level(1)
        # To construct emb_top and emb_bot, we have to combine self.dmp with the
        # the leg_dicts of top and bot.
        # More precisely: emb_top is the composition of the inverse of the leg_dict
        # of top, i.e. top.stratum_number, and self.dmp
        # (giving a map from the points of top to the points of the enveloping
        # stratum of self) and the same for bot.
        # We implement this by iterating over the marked points of self on top level,
        # which are exactly the keys of self.dmp that are on top level.
        # Note that we make extra sure that we didn't mess up the level numbering by
        # using the relative level numbering (where the top level is guaranteed to be 0
        # and the bottom level is 1 (positive!)).
        self._emb_top = {self._top.stratum_number(l): self.dmp[l]
                         for l in iter(self.dmp)
                         if self.LG.level_number(self.LG.levelofleg(l)) == 0}
        self._emb_bot = {self._bot.stratum_number(l): self.dmp[l]
                         for l in iter(self.dmp)
                         if self.LG.level_number(self.LG.levelofleg(l)) == 1}
        # Because this is a BIC, all edges of self are cut in this process
        # and this is exactly the dictionary we must remember
        # WARNING: Here we assume that e[0] is on top level and e[1] is on bottom
        #   This is assured by tidy_up, e.g. after initialisation!
        # Note that all these dictionaries map points of GeneralisedStrata to each
        # other so we must take the corresponding stratum_number!
        self._clutch_dict = {
            self._top.stratum_number(
                e[0]): self._bot.stratum_number(
                e[1]) for e in self.LG.edges}
        return {'X': self.X, 'top': self._top, 'bottom': self._bot,
                'clutch_dict': self._clutch_dict,
                'emb_dict_top': self._emb_top, 'emb_dict_bot': self._emb_bot, }

    def is_legal(self):
        r"""
        Check the R-GRC for self.

        Returns:
            bool: result of R-GRC.
        """
        if any(self.level(l).is_empty()
               for l in range(self.number_of_levels)):
            return False
        return True

    def is_connected(self, with_infty=True):
        r"""
        Check if `self` is connected.
        If `with_infty` is `True`, check if the graph with the level at infinity
        is connected.

        EXAMPLES::

            sage: from admcycles.diffstrata import LevelGraph
            sage: LevelGraph([1,1], [[1],[2]], [], {1:0,2:0}, [0,0]).embedded_level_graph().is_connected()
            False

            sage: LevelGraph([1,1], [[1,3],[2]], [(2,3)], {1:2,2:0,3:-2}, [1,0]).embedded_level_graph().is_connected()
            True

            sage: from admcycles.diffstrata import GeneralisedStratum, Signature
            sage: X = GeneralisedStratum([Signature((0,0,-4)), Signature((0,0,-4))], res_cond=[[(0,2), (1,2)]])
            sage: X.smooth_LG.is_connected()
            True
            sage: X.smooth_LG.is_connected(with_infty=False)
            False
        """
        if with_infty:
            next_l = max(max(ls) for ls in self.LG.legs) + 1
            inf_legs = []
            inf_edges = []
            for rc in self.X.res_cond:
                rc_legs = [self.dmp_inv[p] for p in rc]
                v = []
                for l in rc_legs:
                    v.append(next_l)
                    inf_edges.append((next_l, l))
                    next_l += 1
                inf_legs.append(v)
        else:
            inf_legs = []
            inf_edges = []

        legs_to_verts = {l: i for i, ls in enumerate(self.LG.legs + inf_legs) for l in ls}
        adjacency = defaultdict(list)
        for (l1, l2) in self.LG.edges + inf_edges:
            if l1 in legs_to_verts and l2 in legs_to_verts:
                adjacency[legs_to_verts[l1]].append(legs_to_verts[l2])
                adjacency[legs_to_verts[l2]].append(legs_to_verts[l1])

        not_seen = set(range(1, len(self.LG.legs) + len(inf_legs)))
        seen = [0]
        new = [0]
        while new:
            v = new.pop()
            for vv in adjacency[v]:
                if vv in seen:
                    continue
                else:
                    seen.append(vv)
                    new.append(vv)
                    not_seen.remove(vv)
        return not bool(not_seen)

    def standard_markings(self):
        r"""
        Construct a dictionary for relabelling the markings. A standard labelling will label the legs
        of markings first and then the half edges. If the generalised stratum has only one component,
        the standard label of a marking will be exactly the position of that marking in the signature.

        EXAMPLES::

            sage: from admcycles.diffstrata.generalisedstratum import Stratum
            sage: X=Stratum((1,1))
            sage: X.bics[1]
            EmbeddedLevelGraph(LG=LevelGraph([1, 0],[[1, 2], [3, 4, 5, 6]],[(1, 5), (2, 6)],{1: 0, 2: 0, 3: 1, 4: 1, 5: -2, 6: -2},[0, -1]),dmp={3: (0, 0), 4: (0, 1)},dlevels={0: 0, -1: -1})
            sage: X.bics[1].standard_markings()
            {1: 3, 2: 4, 3: 1, 4: 2, 5: 5, 6: 6}

        """
        n_list = [0 for i in range(self.X.h0_H)]  # list of number of markings on each component of the stratum
        for t in self.dmp_inv:
            n_list[t[0]] += 1

        # list such that the j-th entry is the sum of numbers of
        # markings on the components of smaller indices
        n_sums = [0] + [sum(n_list[i] for i in range(j))
                        for j in range(1, self.X.h0_H)]
        new_leg_dict = {}  # the mapping dict for relabelling the legs
        h = 1
        for i in range(1, len(self.LG.poleorders) + 1):
            if i in self.dmp:
                new_leg_dict[i] = (self.dmp[i][1] +
                                   n_sums[self.dmp[i][0]] + 1)
            else:
                new_leg_dict[i] = len(self.dmp) + h
                h = h + 1
        return new_leg_dict

    def relabel(self, legdict, tidyup=True):
        r"""
        Relabel the EmbeddedLevelGraph by a given dictionary.

        INPUT:

            - legdict (dict): A dictionary indicating the map from old markings
              to the new ones

        EXAMPLES::
            sage: from admcycles.diffstrata.generalisedstratum import Stratum
            sage: X = Stratum((1,1))
            sage: dict1={1: 3, 2: 4, 3: 1, 4: 2, 5: 5, 6: 6}
            sage: X.bics[1].relabel(dict1)
            EmbeddedLevelGraph(LG=LevelGraph([1, 0],[[3, 4], [1, 2, 5, 6]],[(3, 5), (4, 6)],{1: 1, 2: 1, 3: 0, 4: 0, 5: -2, 6: -2},[0, -1]),dmp={1: (0, 0), 2: (0, 1)},dlevels={0: 0, -1: -1})

        """
        newLG = self.LG.relabel(legdict, tidyup)
        new_dmp = {legdict[i]: j for i, j in self.dmp.items()}
        if tidyup:
            new_dmp = dict(sorted(new_dmp.items()))
        newEmbLG = EmbeddedLevelGraph(self.X, newLG, new_dmp, self.dlevels)

        return newEmbLG

    def is_isomorphic(self, other):
        r"""
        Check if self and other are isomorphic (as EmbeddedLevelGraphs).

        Args:
            other (EmbeddedLevelGraph): Graph to check isomorphism.

        Returns:
            bool: True if there exists at least one isomorphism.
        """
        # TODO: Maybe include a way to check against unembedded LGs
        # TODO: Check embedding!
        if not isinstance(other, EmbeddedLevelGraph):
            return False
        try:
            next(self.isomorphisms(other))
            return True
        except StopIteration:
            return False

    @property
    @cached_method
    def quotient_automorphisms(self):
        r"""
        The automorphisms of the quotient graph.

        OUTPUT:

        A list of pairs ``(map_of_vertices, map_of_legs)``. Both ``maps_of_vertices``
        and ``map_of_legs`` are dictionaries.
        """
        quot_LG = self.LG.quotient()
        return list(quot_LG.automorphisms())

    @property
    @cached_method
    def automorphisms(self):
        r"""
        The automorphisms of self (as automorphisms of the underlying LevelGraph,
        respecting the embedding, see doc of isomorphisms).

        OUTPUT:

        A list of pairs ``(map_of_vertices, map_of_legs)``. Both ``maps_of_vertices``
        and ``map_of_legs`` are dictionaries.
        """
        return list(self.isomorphisms(self))

    def isomorphisms(self, other):
        r"""
        Generator yielding the "next" isomorphism of self and other.

        Note that while this gives an "isomorphism" from self.LG to other.LG, this
        is not necessarily an isomorphism of the LevelGraphs (the numbered points may
        be permuted if this is "fixed" by the embedding).

        INPUT:

        other: EmbeddedLevelGraph
        The (potentially) isomorphic EmbeddedLevelGraph.

        OUTPUT:

        An iterator going through isomorphisms. Each isomorphism is encoded by a
        pair of dictionaries ``(vertex_map, leg_map)`` The dictionaries
        ``vertex_map`` (respectively ``leg_map``) is to the mapping of the
        vertices (resp. legs) of ``self.LG`` to the vertices (resp. legs) of
        ``other.LG``.
        """
        # Befor doing the more involved computiations, do some quick heuristics.
        # Compare the genera
        if sorted(self.LG.genera) != sorted(other.LG.genera):
            return
        # Compare the number of adjacent legs
        if sorted(len(ls) for ls in self.LG.legs) != sorted(len(ls) for ls in other.LG.legs):
            return
        # Compare the number of edges
        if len(self.LG.edges) != len(other.LG.edges):
            return
        # Compare the appearing poleorders
        if sorted(self.LG.poleorders.values()) != sorted(other.LG.poleorders.values()):
            return
        # Compare the number of levels
        if self.LG.numberoflevels() != other.LG.numberoflevels():
            return

        # We relabel the underlying level graph of other to fit the labeling
        # of self.
        ldict = {}
        for l_self, p in self.dmp.items():
            l_other = other.dmp_inv[p]
            ldict[l_other] = l_self
        # We need to add the edges to ldict
        next_l = max(ldict.values()) + 1
        for l in itertools.chain(*other.LG.legs):
            if l not in ldict:
                ldict[l] = next_l
                next_l += 1
        other_LG = other.LG.relabel(ldict, tidyup=False)

        # We need to translate the isomorphisms back to the original marking of other
        ldict_inv = {l2: l1 for l1, l2 in ldict.items()}
        for (vmap, lmap) in self.LG.isomorphisms(other_LG):
            lmap_corrected = {l1: ldict_inv[l2] for l1, l2 in lmap.items()}
            yield (vmap, lmap_corrected)

    def fix_deck(self):
        r"""
        If self is a smooth levelgraph, fix the deck transformation of the underlying level graph to match the deck transformation of the ambient stratum.
        """
        if self.LG.edges:
            raise ValueError("Can only fix the deck transformation for the smooth graph")

        new_deck = {l: self.dmp_inv[self.X.deck[self.dmp[l]]]
                    for l in itertools.chain(*self.LG.legs)}
        self.LG.set_deck(self.X.k, new_deck)

    def canonical_quotient(self):
        r"""
        Return the quotient of self where the legs are relabeled to match
        the signature of the quotient stratum in a canonical way.
        More precisely, we define an order on the points (i, j) of the underlying
        stratum as
        (i1, j1) < (i2, j2) iff i1 < i2 or (i1 == i2 and j1 < j2).
        Then we consider the induced order on the G-orbits of the points, i.e.
        O1 < O2 iff min(O1) < min(O2).
        If the G-orbits are O1 < O2 < ... < On, then the leg i of the returned
        graph corresponds to the image of Oi.
        """

        class Pnt:
            def __init__(self, p):
                self.p = p

            def __eq__(self, p2):
                return self.p == p2.p

            def __lt__(self, p2):
                return (
                    (self.p[0] < p2.p[0]) or
                    (self.p[0] == p2.p[0] and self.p[1] < p2.p[1])
                )

            def __hash__(self):
                return hash(self.p)

        class Orbit:
            def __init__(self, o):
                self.o = o
                self.m = min(o)

            def __eq__(self, o2):
                return self.m == o2.m

            def __lt__(self, o2):
                return self.m < o2.m

        orbits = self.X.deck_orbits
        orbits = sorted(Orbit([Pnt(p) for p in o]) for o in orbits)

        quot, lmap = self.LG.quotient_with_map()
        # We first record how we need to rename the legs
        legdict = {}
        for i, o in enumerate(orbits):
            l = self.dmp_inv[o.o[0].p]
            legdict[lmap[l]] = i + 1
        # We need to add the halfedges to the dict
        next_l = len(orbits) + 1
        for l in itertools.chain(*quot.legs):
            if l in legdict:
                continue
            legdict[l] = next_l
            next_l += 1
        return quot.relabel(legdict)

    def _unicode_art_(self):
        r"""
        Return unicode art for ``self``.

        """
        return self.LG._unicode_art_()
