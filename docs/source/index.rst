.. _index:
.. admcycles documentation master file


Welcome to the admcycles documentation!
=======================================

Some examples are available in the :ref:`tutorial`.

.. toctree::
   :maxdepth: 2

   tutorial
   examples
   DRmodule
   admcyclesmodule
   stable_graph
   decorated_graph
   tautological_ring
   double_ramification_cycle
   GRRcomp
   stratarecursion
   integer_list
   file_cache
   diffstrata/additivegenerator
   diffstrata/adm_eval_cache
   diffstrata/auxiliary
   diffstrata/bic
   diffstrata/cache
   diffstrata/elgtautclass
   diffstrata/embeddedlevelgraph
   diffstrata/generalisedstratum
   diffstrata/levelgraph
   diffstrata/sig
   diffstrata/spinstratum
   diffstrata/stratatautring
   diffstrata/xi_cache
   references
