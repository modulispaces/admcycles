{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Norbury (2017) - A new cohomology class on the moduli space of curves\n",
    "\n",
    "In [[Norbury]](https://arxiv.org/pdf/1712.03662v1.pdf) a certain collection of classes $\\Theta_{g,n}$ with good properties are defined. These classes arise as top Chern classes of a bundle coming from the moduli space of stable curves enriched with a certain $2$-spin structure, we refer to the original paper for details.\n",
    "\n",
    "In ``admcycles``, the theta classes can be calculated using the function ``ThetaClass``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from admcycles import *\n",
    "\n",
    "T2 = ThetaClass(2,0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The calculation relies on the Chiodo formula, which is implemented in the function ``DR_cycle``. So we can also manually re-define the function ``ThetaClass`` as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ThetaClass(g, n):\n",
    "    r=2; s=-1; d=2*g - 2 + n; x=-1;\n",
    "    A=tuple(1 for i in [1 .. n]);\n",
    "    return 2**(g - 1 + n)*(r**(2*g - 2*d - 1))*(x^d)*DR_cycle(g,A,d,s,chiodo_coeff=True,r_coeff=r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below, we check the four properties of the Theta classes appearing in the first page of [[Norbury]](https://arxiv.org/pdf/1712.03662v1.pdf).\n",
    "\n",
    "(i) The class $\\Theta_{g,n}$ is of pure degree (equal to $2g-2+n$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "T1 = ThetaClass(3,0);\n",
    "## Check pure of degree\n",
    "deglist = T1.degree_list()\n",
    "len(deglist) == 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "deglist[0]  # equal to 2*3-2+0 = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(ii) The class $\\Theta_{g,n}$ pulls back nicely under boundary gluing morphisms:\n",
    "$$\n",
    "\\xi_{\\mathrm{irr}}^* \\Theta_{g,n} = \\Theta_{g-1,n+2} \\quad \\text{and} \\quad \\xi_{h,I}^* \\Theta_{g,n} = \\pi_1^* \\Theta_{h, |I|+1} \\cdot \\pi_2^* \\Theta_{g-h, |J|+1}\\,.\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from admcycles.admcycles import StableGraph, prodtautclass\n",
    "gamma1 = StableGraph([1,2],[[1],[2]],[(1,2)])\n",
    "LHS = gamma1.boundary_pullback(T1)\n",
    "RHS = prodtautclass(gamma1, protaut = [ThetaClass(1,1), ThetaClass(2,1)])\n",
    "\n",
    "LHS.totensorTautbasis(deglist[0],vecout=True) == RHS.totensorTautbasis(deglist[0],vecout=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On non-separable nodes (first equation of property ii). ) it can be done in a similar way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "T2 = ThetaClass(2,1)\n",
    "gamma2 = StableGraph([1],[[1,2,3]],[(2,3)])\n",
    "PullbackTheta = gamma2.boundary_pullback(T2)\n",
    "dec_list = [p[0] for p in PullbackTheta.terms]\n",
    "LHS = admcycles.tautclass(dec_list)\n",
    "RHS = ThetaClass(1,3)\n",
    "\n",
    "LHS == RHS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(iii) $\\Theta_{g,n+1} = \\psi_{n+1} \\cdot \\pi^* \\Theta_{g,n}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ThetaClass(1,2).forgetful_pullback([3])*psiclass(3,1,3) == ThetaClass(1,3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(iv) $\\Theta_{1,1} = 3 \\psi_1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ThetaClass(1,1) == 3*psiclass(1,1,1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.7",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
