.. _stratarecursion_module:

Module ``stratarecursion``
==========================

.. automodule:: admcycles.stratarecursion
   :members:
   :undoc-members:
   :show-inheritance:
