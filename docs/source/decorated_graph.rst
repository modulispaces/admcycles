.. _decorated_graph:

Module ``decorated graph``
==========================

.. automodule:: admcycles.decorated_graph
  :members:
  :undoc-members:
  :show-inheritance:
